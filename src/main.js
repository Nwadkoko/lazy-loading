var source   = document.getElementById("button1");
source.addEventListener('click', getText);
function getText() {
    // Grab the template script
    var theTemplateScript = document.getElementById("entry-template").innerHTML;

    // Compile the template
    var theTemplate = Handlebars.compile(theTemplateScript);

    // Define our data object
    var context={
        "var" : ""
    };

    // Pass our data to the template
    var theCompiledHtml = theTemplate(context);

    // Add the compiled html to the page
    document.getElementById('content-placeholder').innerHTML += theCompiledHtml;

    echo.init();
}